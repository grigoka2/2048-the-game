# 2048 Game

Welcome to the 2048 Game! This is a console-based implementation of the popular 2048 game. Use your arrow keys to move tiles, and combine matching tiles to reach the target value for victory.

## Commands

- `/help`: Display game instructions and available commands.
- `/start`: Start a new game. You will be prompted to enter the target value for victory.
- `/viewrecord`: View the current best moves record.
- `/resetrecord`: Reset the best moves record to INT_MAX.
- `/exit`: Exit the game.

## Instructions

1. **Starting the Game:**
    - Type `/start` to begin a new game.
    - Enter the target value for victory (8, 16, 32, ..., 2048) when prompted.

2. **Playing the Game:**
    - Use 'w' to move tiles up, 's' to move down, 'a' to move left, and 'd' to move right.
    - Try to combine matching numbers to reach the target value for victory.
    - The game ends when you reach the target value or when there are no valid moves left.

3. **Viewing Best Moves Record:**
    - Type `/viewrecord` to see the current best moves record.

4. **Resetting Best Moves Record:**
    - Type `/resetrecord` to reset the best moves record to INT_MAX.

5. **Exiting the Game:**
    - Type `/exit` to exit the game.

6. **Exiting Mid-Game:**
    - During the game, type '/z' followed by 'Enter' to end the game.

Have fun playing 2048! If you have any questions or issues, feel free to reach out.