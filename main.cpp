#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <climits>
#include <fstream>

using namespace std;

const int SIZE = 4;

vector<vector<int>> board(SIZE, vector<int>(SIZE, 0));

int movesCount = 0;
int bestMoves = INT_MAX;
int targetValue = 2048;

// Function to display the game board
void displayBoard() {
    cout << "2048 Game" << endl;
    cout << "==========================" << endl;
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (board[i][j] == 0) {
                cout << ".\t";
            } else {
                cout << board[i][j] << "\t";
            }
        }
        cout << endl;
    }
    cout << "==========================" << endl;
    cout << "Moves: " << movesCount << "\tBest: " << bestMoves << endl;
}

// Function to generate a new number on the board
void generateNumber() {
    int emptyCount = 0;
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (board[i][j] == 0) {
                emptyCount++;
            }
        }
    }
    if (emptyCount == 0) {
        return;
    }
    int position = rand() % emptyCount + 1;
    int target = rand() % 10 < 9 ? 2 : 4;
    emptyCount = 0;
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (board[i][j] == 0) {
                emptyCount++;
                if (emptyCount == position) {
                    board[i][j] = target;
                    return;
                }
            }
        }
    }
}

// Function to shift elements to the left
void moveLeft() {
    for (int i = 0; i < SIZE; i++) {
        int current = 0; // Index for inserting non-zero elements
        for (int j = 0; j < SIZE; j++) {
            if (board[i][j] != 0) {
                // Search for adjacent identical elements for merging
                int k = j + 1;
                while (k < SIZE && board[i][k] == 0) {
                    k++;
                }
                if (k < SIZE && board[i][j] == board[i][k]) {
                    board[i][current++] = 2 * board[i][j]; // Merge
                    j = k; // Skip the next element, as it has already been merged
                } else {
                    board[i][current++] = board[i][j];
                }
            }
        }
        while (current < SIZE) {
            board[i][current++] = 0;
        }
    }
}

// Function to shift elements to the right
void moveRight() {
    for (int i = 0; i < SIZE; i++) {
        int current = SIZE - 1; // Index for inserting non-zero elements
        for (int j = SIZE - 1; j >= 0; j--) {
            if (board[i][j] != 0) {
                // Search for adjacent identical elements for merging
                int k = j - 1;
                while (k >= 0 && board[i][k] == 0) {
                    k--;
                }
                if (k >= 0 && board[i][j] == board[i][k]) {
                    board[i][current--] = 2 * board[i][j]; // Merge
                    j = k; // Skip the previous element, as it has already been merged
                } else {
                    board[i][current--] = board[i][j];
                }
            }
        }
        while (current >= 0) {
            board[i][current--] = 0;
        }
    }
}

// Function to shift elements upwards
void moveUp() {
    for (int j = 0; j < SIZE; j++) {
        int current = 0; // Index for inserting non-zero elements
        for (int i = 0; i < SIZE; i++) {
            if (board[i][j] != 0) {
                // Search for adjacent identical elements for merging
                int k = i + 1;
                while (k < SIZE && board[k][j] == 0) {
                    k++;
                }
                if (k < SIZE && board[i][j] == board[k][j]) {
                    board[current++][j] = 2 * board[i][j]; // Merge
                    i = k; // Skip the next element, as it has already been merged
                } else {
                    board[current++][j] = board[i][j];
                }
            }
        }
        while (current < SIZE) {
            board[current++][j] = 0;
        }
    }
}

// Function to shift elements downwards
void moveDown() {
    for (int j = 0; j < SIZE; j++) {
        int current = SIZE - 1; // Index for inserting non-zero elements
        for (int i = SIZE - 1; i >= 0; i--) {
            if (board[i][j] != 0) {
                // Search for adjacent identical elements for merging
                int k = i - 1;
                while (k >= 0 && board[k][j] == 0) {
                    k--;
                }
                if (k >= 0 && board[i][j] == board[k][j]) {
                    board[current--][j] = 2 * board[i][j]; // Merge
                    i = k; // Skip the previous element, as it has already been merged
                } else {
                    board[current--][j] = board[i][j];
                }
            }
        }
        while (current >= 0) {
            board[current--][j] = 0;
        }
    }
}

// Function to check if you win
bool checkWin() {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (board[i][j] == targetValue) {
                return true;
            }
        }
    }
    return false;
}

// Function to check if you lose
bool checkLose() {
    // Check for empty space
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (board[i][j] == 0) {
                return false;
            }
        }
    }

// Check for possible merging of numbers
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if ((i < SIZE - 1 && board[i][j] == board[i + 1][j]) ||
                (j < SIZE - 1 && board[i][j] == board[i][j + 1])) {
                return false;
            }
        }
    }

    return true;
}

void displayHelp() {
    cout << "2048 Game Instructions:" << endl;
    cout << "==========================" << endl;
    cout << "Use 'w' to move up, 's' to move down, 'a' to move left, and 'd' to move right." << endl;
    cout << "Try to combine matching numbers to reach 2048!" << endl;
    cout << "To view the best moves record, type '/viewrecord'." << endl;
    cout << "To reset the best moves record, type '/resetrecord'." << endl;
    cout << "To exit, type '/exit' or close the console window." << endl;
    cout << "==========================" << endl;
}

// Function to display a welcome message
void displayWelcome() {
    cout << R"(
   /\\\\\\\\\          /\\\\\\\                /\\\         /\\\\\\\\\
  /\\\///////\\\      /\\\/////\\\            /\\\\\       /\\\///////\\\
  \///      \//\\\    /\\\    \//\\\         /\\\/\\\      \/\\\     \/\\\
             /\\\/    \/\\\     \/\\\       /\\\/\/\\\      \///\\\\\\\\\/
           /\\\//      \/\\\     \/\\\     /\\\/  \/\\\       /\\\///////\\\
         /\\\//         \/\\\     \/\\\   /\\\\\\\\\\\\\\\\   /\\\      \//\\\
        /\\\/            \//\\\    /\\\   \///////////\\\//   \//\\\      /\\\
        /\\\\\\\\\\\\\\\   \///\\\\\\\/              \/\\\      \///\\\\\\\\\/
        \///////////////      \///////                \///         \/////////
  )" << endl;
    cout << "Welcome to the 2048 Game!" << endl;
    cout << "Type '/help' for instructions or '/start' to begin the game." << endl;
}

void saveBestMoves() {
    ofstream file("best_moves.txt");
    if (file.is_open()) {
        file << bestMoves;
        file.close();
    } else {
        cout << "Unable to save bestMoves to file." << endl;
    }
}

// Function to load the bestMoves from a file
void loadBestMoves() {
    ifstream file("best_moves.txt");
    if (file.is_open()) {
        file >> bestMoves;
        file.close();
    } else {
        cout << "Unable to load bestMoves from file. Setting it to INT_MAX." << endl;
        bestMoves = INT_MAX;
    }
}

// Function to view the bestMoves record
void viewBestMovesRecord() {
    cout << "Current Best Moves Record: " << (bestMoves == INT_MAX ? "No record set" : to_string(bestMoves)) << endl;
}

// Function to reset the bestMoves record
void resetBestMovesRecord() {
    bestMoves = INT_MAX;
    saveBestMoves();
    cout << "Best Moves Record has been reset to INT_MAX." << endl;
}


int main() {
    srand(time(NULL));

    loadBestMoves();

    displayWelcome();

    string command;
    while (true) {
        cout << "Enter a command: ";
        cin >> command;


        if (command == "/help") {
            displayHelp();
        } else if (command == "/start") {
        cout << "Enter the target value for victory (8, 16, 32, ..., 2048): ";
        cin >> targetValue;
        break;  // Exit the loop and start the game
        } else if (command == "/viewrecord") {
            // View best moves record
            viewBestMovesRecord();
        } else if (command == "/resetrecord") {
            // Reset best moves record
            resetBestMovesRecord();
        } else if (command == "/exit") {
            cout << "Exiting the game. Goodbye!" << endl;
            return 0;
        } else {
            cout << "Invalid command. Type '/help' for instructions or '/start' to begin the game." << endl;
        }
    }

    generateNumber();
    generateNumber();
    displayBoard();

    while (true) {
        char move;
        cin >> move;

        switch (move) {
            case 'w':
                moveUp();
                break;
            case 's':
                moveDown();
                break;
            case 'a':
                moveLeft();
                break;
            case 'd':
                moveRight();
                break;
            default:
                if (move == '/' && cin.peek() == 'z') {
                    cout << "Game ended by user." << endl;
                    return 0;
                } else {
                    cout << "Invalid move! Use 'w', 's', 'a', or 'd'. Type '/help' for instructions." << endl;
                    continue;
                }
        }

        movesCount++;

        if (checkWin()) {
            cout << "Congratulations! You win!" << endl;
            if (movesCount < bestMoves) {
                bestMoves = movesCount;
                saveBestMoves();
            }
            break;
        }

        if (checkLose()) {
            cout << "Game over! You lose!" << endl;
            break;
        }

        generateNumber();
        displayBoard();
    }

    return 0;
}